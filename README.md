# README #

Helpers for development with the Django framework.

Gives you access to manage.py from anywhere in the project with the dm command.

### Setup ###

* Clone repository to your ZSH custom plugins folder (Usually ~/.oh-my-zsh/custom/plugins)
* Add the plugin to your activated plugins list in .zshrc
* Restart your terminal, or launch a new shell

### Usage ###

Using the dm command without arguments displays the help. The following commands are equal:
```shell
$ dm
$ python manage.py help
```

All other arguments work as expected. The following commands are equal:
```shell
$ dm test
$ python manage.py test
```

There is also a short version for django-admin:
```shell
$ da
```

And a short version for python:
```shell
$ py
```