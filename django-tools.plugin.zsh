# Alias
alias da='django-admin'
alias py='python'

# Reference manage.py from anywhere in the project
function dm() {
	# Use help command by default
	if [ $# -eq 0 ]; then
		1='help'
	fi

	while true; do
		# Found manage.py
		if [ -f 'manage.py' ]; then
			python manage.py "$*"
			break
		fi

		# Reached root folder
		if [ $(pwd) = '/' ]; then
			echo 'Not a django project.'
			break
		fi

		cd ..
	done
}
